# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/gnome-kiosk/issues
# Bug-Submit: https://github.com/<user>/gnome-kiosk/issues/new
# Changelog: https://github.com/<user>/gnome-kiosk/blob/master/CHANGES
# Documentation: https://github.com/<user>/gnome-kiosk/wiki
# Repository-Browse: https://github.com/<user>/gnome-kiosk
# Repository: https://github.com/<user>/gnome-kiosk.git
